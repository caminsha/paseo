# Paseo

An open source step counting application for Android.

<img src="screenshots/Screenshot_Dashboard_1.png" alt="Paseo Dashboard" width="200px">
<img src="screenshots/Screenshot_Dashboard_2.png" alt="Paseo Dashboard - extra info" width="200px">

<img src="screenshots/Screenshot_Step_Summary_Day_1.png" alt="Paseo Daily Steps Summary" width="200px">
<img src="screenshots/Screenshot_Step_Summary_Day_2.png" alt="Paseo Daily Steps Summary - with graph" width="200px">

## Libraries Used
* [MPAndroidChart][0] - for displaying bargraphs of steps by time period.
* [FloatSeekBarPreference][1] - modified for formatted seekbarpreference

[0]: https://github.com/PhilJay/MPAndroidChart
[1]: https://gist.github.com/yuntan/637b7489bcb9d8e1b01026530dd1e1e9

## License

Paseo is distributed under the MIT [license](LICENSE)
