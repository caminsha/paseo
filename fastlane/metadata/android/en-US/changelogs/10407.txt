January 23, 2022 Commit

Bug fixes including:
 - dark and black themes now display correctly on Android 12
 - importing/exporting database now succeeds on Android 12
 - importing/exporting can be done from any shared folder on device
 - navigation drawer correctly shown (not behind status bar)