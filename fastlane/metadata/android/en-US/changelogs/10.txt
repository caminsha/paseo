Bug fixes:
 - fixed crash when entering 'settings' when device language is other than English

Still in progress:
 - will not (yet) install on devices that do not have a step counter sensor.