package ca.chancehorizon.paseo


import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import ca.chancehorizon.paseo.databinding.FragmentEditStepsBinding
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.widget.DatePicker
import androidx.annotation.ColorInt
import java.text.DateFormat


class EditStepsFragment : androidx.fragment.app.Fragment(), OnDateSetListener {

    // timeUnit is used to display the correct screen when dismissing (pressing back) from the edit steps screen
    var timeUnit: String = "???"
        get() = field        // getter
        set(value) {         // setterfirst run dialog again
            field = value
        }

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var fragmentEditStepsBinding: FragmentEditStepsBinding? = null
    private var editStepsCalendar: Calendar = Calendar.getInstance()

    var theDate: Int = SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(editStepsCalendar.time).toInt()
        get() = field        // getter
        set(value) {         // setterfirst run dialog again
            field = value
            val year = theDate / 10000
            val month = (theDate - year*10000) / 100
            val dayOfMonth = (theDate - year*10000 - month*100)
            editStepsCalendar.set(year, month - 1, dayOfMonth) // kotlin/java month is zero-based
        }

    lateinit var paseoDBHelper : PaseoDBHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_edit_steps, container, false)

        val binding = FragmentEditStepsBinding.bind(view)
        fragmentEditStepsBinding = binding

        fragmentEditStepsBinding!!.date.text = DateFormat.getDateInstance(DateFormat.FULL).format(editStepsCalendar.time)

        //createStepsTable(theDate)

        fragmentEditStepsBinding!!.date.setOnClickListener {
            val datePickerDialog =
                    DatePickerDialog(this.requireContext(), R.style.PaseoDialog2,this,
                            editStepsCalendar.get(Calendar.YEAR),
                            editStepsCalendar.get(Calendar.MONTH),
                            editStepsCalendar.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.show()
        }


        // let the main activity of Paseo know which help screen to show for the edit steps fragment
        (activity as MainActivity).theScreen = "editSteps"

        // let the main activity of Paseo know which screen to show when the edit steps screen is dismissed
        (activity as MainActivity).restoreScreen = timeUnit

        return view
    }



    fun createStepsTable(date : Int = theDate) {

        var recordedSteps: Int
        var editedSteps: Int
        var editFlag: Boolean

        // get steps for every time unit from the steps table in the database
        val stepsArray: ArrayList<Array<Int>> = paseoDBHelper.getEditedStepsByHour(date, true)

        // first remove everything from the table to make sure it is empty
        //  this prevents unintentionally adding rows to old table content
        fragmentEditStepsBinding!!.editStepsTable.removeAllViews()

        val headerRow = TableRow(context)
        headerRow.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)

        val hourTitleCell = TextView(context)
        hourTitleCell.apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT)
            text = getString(R.string.hour)
        }
        headerRow.addView(hourTitleCell)

        val stepsTitleCell = TextView(context)
        stepsTitleCell.apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT)
            text = context.getString(R.string.recorded_steps)
        }
        headerRow.addView(stepsTitleCell)

        val editStepsTitleCell = TextView(context)
        editStepsTitleCell.apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT)
            text = context.getString(R.string.entered_steps)
        }
        headerRow.addView(editStepsTitleCell)

        fragmentEditStepsBinding!!.editStepsTable.addView(headerRow)

        // add a row in the table for each unit of time so far
        var stepTime = 0
        // add a row in the table for every hour in the day (even if no steps were recorded for that hour)
        for (hour in 0 until 24) {

            recordedSteps = 0
            editedSteps = 0
            editFlag = false

            // there are only entries in the stepsArray when either recorded or edited steps are not zero
            if (stepsArray.size > 0 && stepsArray.size > stepTime) {
                if (stepsArray[stepTime][0] == hour) {
                    recordedSteps = stepsArray[stepTime][1]
                    editedSteps = stepsArray[stepTime][2]
                    editFlag = stepsArray[stepTime][4] == 1
                    stepTime = stepTime + 1
                }
            }

            val row = TableRow(context)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)

            // set up the time value for the time cell in the table
            val timeCell = TextView(context)
            timeCell.apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT)

                // format the entry in the first column
                //  (the time/date of the number of steps)
                val theText = hour.toString() + "  "
                text = theText
            }
            row.addView(timeCell)

            // enter the number of steps for this unit of time in the table
            val stepsCell = TextView(context)
            stepsCell.apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT)
                text = NumberFormat.getIntegerInstance().format(recordedSteps)
            }
            stepsCell.gravity = Gravity.CENTER_HORIZONTAL
            row.addView(stepsCell)

            // enter the number of steps for this unit of time in the table
            val editCell = TextView(context)

            editCell.apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT)
                if (editedSteps != 0) {
                    text = NumberFormat.getIntegerInstance().format(editedSteps + recordedSteps)
                }
                else {
                    text = " - "
                }
            }
            if (editFlag) {
                @ColorInt val textColor = resolveColorAttr(requireView().context,
                        android.R.attr.colorSecondary)

                editCell.setTextColor(textColor)
            }

            editCell.gravity = Gravity.CENTER_HORIZONTAL

            // respond to user touching the edited steps field (so that the user can manually override the number of
            //  steps recorded by the device
            row.setOnClickListener {
                // fill in all the details in the edit steps bottom sheet
                val bottomSheet = EditStepsEntryFragment()

                val editDate = DateFormat.getDateInstance(DateFormat.FULL).format(editStepsCalendar.time)
                var editSteps : Int
                try {
                    editSteps = editCell.text.toString().getAmount().toInt()
                }
                catch (nfe: NumberFormatException) {
                    editSteps = 0
                }

                bottomSheet.displayDate = editDate + ", " + hour + "h"
                bottomSheet.date = theDate
                bottomSheet.hour = hour
                bottomSheet.recordedSteps = stepsCell.text.toString().getAmount().toInt()
                bottomSheet.editedSteps = editSteps

                bottomSheet.parent = bottomSheet.parentFragment
                bottomSheet.show(childFragmentManager, "BottomSheet")
            }
            row.addView(editCell)

            fragmentEditStepsBinding!!.editStepsTable.addView(row, 1)
        }
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {

        createStepsTable(theDate)

        super.onResume()
    }



    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        fragmentEditStepsBinding = null

        super.onDestroy()
    }



    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        // Set static variables of Calendar instance
        editStepsCalendar[Calendar.YEAR] = year
        editStepsCalendar[Calendar.MONTH] = month
        editStepsCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth

        // Get the date in form of string
        val selectedDate: String = DateFormat.getDateInstance(DateFormat.FULL).format(editStepsCalendar.time)

        // Set the textview to the selectedDate String
        fragmentEditStepsBinding!!.date.text = selectedDate

        val theDateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()) // looks like "19891225"
        theDate = theDateFormat.format(editStepsCalendar.time).toInt()

        createStepsTable(theDate)
    }


    fun String.getAmount(): String {
        return replace("[^0-9]".toRegex(), "")
    }
}

