package ca.chancehorizon.paseo


import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper

//import kotlinx.android.synthetic.main.dashboard_legend_bottomsheet.*
import ca.chancehorizon.paseo.databinding.DashboardLegendBottomsheetBinding



class DashboardLegendFragment : BottomSheetDialogFragment(), View.OnClickListener {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var dashboardLegendBinding: DashboardLegendBottomsheetBinding? = null

    private var mListener: ItemClickListener? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val theTheme = requireContext().getTheme()
        val contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view = inflater.cloneInContext(contextThemeWrapper).inflate(R.layout.dashboard_legend_bottomsheet, container, false)

        val binding = DashboardLegendBottomsheetBinding.bind(view)
        dashboardLegendBinding = binding

        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // respond to the user tapping anywhere in the dialog
        dashboardLegendBinding!!.legendLayout.setOnClickListener(this)
    }



    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        dashboardLegendBinding = null

        super.onDestroy()
    }




//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//        if (context is ItemClickListener) {
//            mListener = context
//        } else {
//            throw RuntimeException(context!!.toString() + " must implement ItemClickListener")
//        }
//    }
//
//    override fun onDetach() {
//        super.onDetach()
//        mListener = null
//    }



    // respond to user taps
    override fun onClick(view: View) {

        // close the dialog
        dismiss()
    }



    // *** can remove ???
    interface ItemClickListener {
        fun onItemClick(item: String)
    }


    //  *** can remove ???
    companion object {

        val TAG = "ActionBottomDialog"

        fun newInstance(): DashboardLegendFragment {
            return DashboardLegendFragment()
        }
    }
}